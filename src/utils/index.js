exports.makeLogPath = ({ slug }) => {
  return `/logs/${slug}`;
};

exports.makeProjectPath = ({ slug }) => {
  return `/projects/${slug}`;
};

exports.makeWikiPath = ({ frontmatter }) => {
  return `/wiki/${frontmatter.slug}`;
};
